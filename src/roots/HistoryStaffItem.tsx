import * as React from 'react';
import '@vkontakte/vkui/dist/vkui.css';
import {inject, observer} from "mobx-react";
import RootStore from '../stores/RootStore';
import {List, Group, Header, Cell, Separator} from '@vkontakte/vkui';

@inject('RootStore')
@observer
class HistoryStaffItem extends React.Component<{ event : MeetingPast, RootStore? : RootStore }, {}> {
    render(): React.ReactElement {
        const event = this.props.event;
        const {UIStore} = this.props.RootStore!;

        return (
            <Cell
                key={event.id}
                description={((event.wasMark === undefined) || (event.usefulMark === undefined) || (event.timingMark === undefined)) &&
								<div style={{color: 'var(--accent)'}}>Оцените мероприятие</div>
                }
                onClick={UIStore.setModal.bind(null,'eventMark'+event.id)}
            >
                {event.title}
            </Cell>
        )
    }
}

export default HistoryStaffItem;
