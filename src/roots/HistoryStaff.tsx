import * as React from 'react';
import '@vkontakte/vkui/dist/vkui.css';
import {inject, observer} from "mobx-react";
import RootStore from '../stores/RootStore';
import {List, Group, Header, Cell, Separator} from '@vkontakte/vkui';
import HistoryStaffItem from "./HistoryStaffItem";

@inject('RootStore')
@observer
class HistoryStaff extends React.Component<{ RootStore?: RootStore }, {}> {
    render(): React.ReactElement {
        const RootStore = this.props.RootStore!;
        const {UIStore} = RootStore;
        const {EmulateStore} = RootStore;

        return (
            <List>
                <Group header={<Header mode="primary">Прошедшие мероприятия</Header>}>
                    <Header mode='secondary'>На этой неделе</Header>
                    {EmulateStore.fakeStaffHistoryWeek.map(event=>
                        <HistoryStaffItem key={event.id} event={event}/>
                    )}
                    <Header mode='secondary'>В этом месяце</Header>
                    {EmulateStore.fakeStaffHistoryMonth.map(event=>
                        <HistoryStaffItem key={event.id} event={event}/>
                    )}
                    <Separator/>
                </Group>
            </List>
        )
    }
}

export default HistoryStaff;
