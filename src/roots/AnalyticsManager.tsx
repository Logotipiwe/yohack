import * as React from 'react';
import '@vkontakte/vkui/dist/vkui.css';
import {inject, observer} from "mobx-react";
import RootStore from '../stores/RootStore';
import Icon24MoreHorizontal from '@vkontakte/icons/dist/24/more_horizontal'
import {List, Group, Header, Cell} from '@vkontakte/vkui';
import dayjs from "dayjs";

@inject('RootStore')
@observer
class AnalyticsManager extends React.Component<{ RootStore?: RootStore }, {}> {
    render(): React.ReactElement {
        const RootStore = this.props.RootStore!;
        const {UIStore} = RootStore;
        const {EmulateStore} = RootStore;

        return (
            <List>
                <Group header={<Header mode="secondary">Прошедшие мероприятия</Header>}>
                    {EmulateStore.fakeManagerHistory.map(event => {
                        return (
                            <Cell
                                onClick={UIStore.setModal.bind(null,'anal'+event.id)}
                                key={event.title}
                                asideContent={<Icon24MoreHorizontal/>}
                                description={dayjs(event.time).format('D MMM YYYY hh:mm')}
                            >
                                {event.title}
                            </Cell>
                        )
                    })}
                </Group>
            </List>
        )
    }
}

export default AnalyticsManager;
