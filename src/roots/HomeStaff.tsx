import * as React from 'react';
import '@vkontakte/vkui/dist/vkui.css';
import {inject, observer} from "mobx-react";
import RootStore from '../stores/RootStore';
import Icon24MoreHorizontal from '@vkontakte/icons/dist/24/more_horizontal'
import {List, Group, Header, Cell, Separator, CellButton, Button, Counter} from '@vkontakte/vkui';

@inject('RootStore')
@observer
class HomeStaff extends React.Component<{ RootStore?: RootStore }, {}> {
    render(): React.ReactElement {
        const RootStore = this.props.RootStore!;
        const {EmulateStore} = RootStore;

        return (
            <List>
                <Group header={<Header mode="secondary" aside={
                    EmulateStore.fakeStuffInvitesCount ?
                        <Counter size="m" mode="prominent">{EmulateStore.fakeStuffInvitesCount}</Counter> : ''
                }>Мои приглашения</Header>}>
                    {EmulateStore.fakeStuffInvitesNotReviewed.map((event) => <Cell
                            size="l"
                            key={event.id}
                            description="Описание, пункты для обсуждения и цели"
                            asideContent={<Icon24MoreHorizontal/>}
                            bottomContent={
                                <div style={{display: 'flex'}}>
                                    <Button
                                        size="l"
                                        mode='commerce'
                                        onClick={EmulateStore.handleStuffInvite.bind(null, event, true)}
                                    >Подтвердить участие</Button>
                                    {!(event.required) ? <Button
                                        size="m"
                                        mode="secondary"
                                        style={{marginLeft: 8}}
                                        onClick={EmulateStore.handleStuffInvite.bind(null,event,false)}
                                    >
                                        Отклонить
                                    </Button> : null
                                    }
                                </div>
                            }
                        >
                            {event.title}
                        </Cell>
                    )}
                    <Separator/>
                    <Header mode='secondary'>Предстоящие встречи</Header>
                    {EmulateStore.fakeStuffInvitesAccepted.map((event, i) => <Cell
                            size="l"
                            key={event.id}
                            description="Описание, пункты для обсуждения и цели"
                            asideContent={<Icon24MoreHorizontal/>}
                        >
                            {event.title}
                        </Cell>
                    )}
                </Group>
            </List>
        )
    }
}

export default HomeStaff;
