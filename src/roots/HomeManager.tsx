import * as React from 'react';
import '@vkontakte/vkui/dist/vkui.css';
import {inject, observer} from "mobx-react";
import RootStore from '../stores/RootStore';
import Icon24Add from '@vkontakte/icons/dist/24/add'
import {List, Group, Header, Cell, Separator, CellButton} from '@vkontakte/vkui';

@inject('RootStore')
@observer
class HomeManager extends React.Component<{ RootStore?: RootStore }, {}> {
    render(): React.ReactElement {
        const RootStore = this.props.RootStore!;
        const {UIStore} = RootStore;
        const {EmulateStore} = RootStore;

        return (
            <List>
                <Group header={<Header mode="secondary">Мои мероприятия</Header>}>
                    {EmulateStore.fakeManagerEvents.map(event=>
                        <Cell
                            key={event.id}
                        >{event.title}</Cell>
                    )}
                    <CellButton before={<Icon24Add/>}>Добавить мероприятие</CellButton>
                    <Separator/>
                </Group>
            </List>
        )
    }
}

export default HomeManager;
