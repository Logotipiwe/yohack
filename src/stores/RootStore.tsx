import {action, computed, observable} from "mobx";
import UIStore from "./UIStore";
import React from "react";
import EmulateStore from "./EmulateStore";

class RootStore {
	constructor() {
		this.EmulateStore = new EmulateStore(this);
		this.UIStore = new UIStore(this);
	}

	EmulateStore: EmulateStore;
	UIStore: UIStore;

	@observable isManager = false;
	@observable url = (process.env.NODE_ENV === 'development') ? '' : '';
	@observable auth = false;
	@observable dataLoaded = false;

	objToGet = (get_data : any) : string => {
		let getArr = [];
		let get = "";
		if (Object.keys(get_data).length) {
			for (let key in get_data) {
				if(get_data.hasOwnProperty(key)) getArr.push(key + '=' + get_data[key]);
			}
			get = '?' + getArr.join('&');
		}
		return get;
	};

}

export default RootStore;
