import RootStore from "./RootStore";
import {action, computed, observable} from "mobx";

class EmulateStore {
    constructor(RootStore : RootStore) {
        this.RootStore = RootStore;
        (process.env.NODE_ENV === 'development') && this.fakeLogin(true);
    }
    RootStore: RootStore;

    @observable fakeStuffInvites : Invite[] = [
        {
            id: 1,
            title: 'Совет директоров',
            time: new Date(2020,3,14),
            required: true,
            accepted: null,
        },
        {
            id: 2,
            title: 'Пятиминутка',
            time: new Date(2020,2,16),
            required: false,
            accepted: null
        },
        {
            id: 3,
            title: 'Собрание 3',
            time: new Date(2020,5,15),
            required: false,
            accepted: null
        },
    ];

    @computed get fakeStuffInvitesCount(){
        return this.fakeStuffInvitesNotReviewed.length;
    }

    @computed get fakeStuffInvitesNotReviewed(){
        return this.fakeStuffInvites.filter(event=>event.accepted===null);
    }

    @computed get fakeStuffInvitesAccepted(){
        return this.fakeStuffInvites.filter(event=>event.accepted===true);
    }

    @action.bound handleStuffInvite(event : Invite, val: boolean){
        event.accepted = val;
    }

    @observable fakeStaffHistory : MeetingPast[] = [
        {
            id: 1,
            time: new Date(2020,4,13),
            title: "Пятиминутка",
        },
        {
            id: 2,
            time: new Date(2020,4,13),
            title: "Совет дерикторов",
            timingMark: 1,
            usefulMark: 1,
            wasMark: 1
        },
        {
            id: 3,
            time: new Date(2020,4,13),
            title: "Просмотр мемов",
            timingMark: 1,
            usefulMark: 1,
            wasMark: 1
        },
        {
            id: 4,
            time: new Date(2020,4,13),
            title: "Просмотр мемов отделом кадров",
            timingMark: 1,
            usefulMark: 1,
            wasMark: 1
        },
        {
            id: 5,
            time: new Date(2020,4,13),
            title: "Срочно митинг решаем будущее компании",
            timingMark: 1,
            usefulMark: 1,
            wasMark: 1
        }
    ];

    @action.bound markMeeting(event : MeetingPast,e : any){
        const name : any = e.target.name;
        const val = parseInt(e.target.value);
        if(name === 'usefulMark') event.usefulMark = val;
        else if(name === 'timingMark') event.timingMark = val;
        else if(name === 'wasMark') event.wasMark = val;
    }

    @computed get fakeStaffHistoryWeek(){
        return this.fakeStaffHistory.slice(0,3);
    }

    @computed get fakeStaffHistoryMonth(){
        return this.fakeStaffHistory.slice(3);
    }

    @observable fakeStuffEvents : Array<Meeting> = [
        {
            id: 1,
            title: 'Неформальное собрание',
            time: new Date(2020,3,14)
        },
        {
            id: 2,
            title: 'Пятиминутка',
            time: new Date(2020,2,16)
        },
        {
            id: 3,
            title: 'Встреча отдела',
            time: new Date(2020,5,15)
        },
    ];

    @observable fakeManagerEvents : Meeting[] = [
        {
            id: 1,
            title: 'Планерка',
            time: new Date(2020,3,14)
        },
        {
            id: 2,
            title: 'Онлайн обед',
            time: new Date(2020,2,16)
        },
        {
            id: 3,
            title: 'Неформальная встреча',
            time: new Date(2020,5,15)
        },
    ];

    @observable fakeManagerHistory : MeetingAnalytics[] = [
        {
            id: 1,
            time: new Date(2020,4,13),
            title: "Утреннее собрание",
            wasConversion: 80,
            usefulGisto: [1,0,0,5,12],
            timingGisto: [0,0,2,16]
        },
        {
            id: 2,
            time: new Date(2020,4,13),
            title: "Совет дерикторов",
            wasConversion: 80,
            usefulGisto: [1,0,0,5,12],
            timingGisto: [0,0,2,16]
        },
        {
            id: 3,
            time: new Date(2020,4,13),
            title: "Просмотр мемов",
            wasConversion: 80,
            usefulGisto: [1,0,0,5,12],
            timingGisto: [0,0,2,16]
        },
        {
            id: 4,
            time: new Date(2020,4,13),
            title: "Что делать дальше",
            wasConversion: 80,
            usefulGisto: [1,0,0,5,12],
            timingGisto: [0,0,2,16]
        },
        {
            id: 5,
            time: new Date(2020,4,13),
            title: "Срочно митинг решаем будущее компании",
            wasConversion: 80,
            usefulGisto: [1,0,0,5,12],
            timingGisto: [0,0,2,16]
        }
    ];

    @action.bound fakeLogin(isManager : boolean){
        this.RootStore.auth = true;
        this.RootStore.isManager = isManager;
        this.RootStore.dataLoaded = true;
    }

    @action.bound logout(){
        this.RootStore.auth = false;
    }

    @action.bound login(){
        const login = this.RootStore.UIStore.LoginUI.inputLogin;
        if(login === 'manager'){
            this.fakeLogin(true);
        } else if (login === 'stuff'){
            this.fakeLogin(false);
        }
    }
}

export default EmulateStore;
