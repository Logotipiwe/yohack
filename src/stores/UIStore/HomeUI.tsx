import {action, observable} from "mobx";
import UIStore from '../UIStore'

export default class HomeUI {
	constructor(UIStore : UIStore) {
		this.UIStore = UIStore;
	}

	UIStore : UIStore;
}
