import {action, observable} from "mobx";
import UIStore from "../UIStore";

export default class LoginUI {
    constructor(UIStore: UIStore) {
        this.UIStore = UIStore;
    }

    UIStore: UIStore;

    @observable activeView: LoginView = 'login';
    @observable inputLogin = '';
    @observable inputPassword = '';
    @observable inputLoginReg = '';
    @observable inputPasswordReg = '';

    @action.bound setInputLogin(e: any) {
        this.inputLogin = e.target.value;
    }

    @action.bound setInputPassword(e: any) {
        this.inputPassword = e.target.value;
    }

    @action.bound submitLogin() {
    }

    @action.bound setActiveView(val : LoginView) {
        this.activeView = val;
    }

    @action.bound setInputLoginReg(e : any) {
        this.inputLoginReg = e.target.value;
    }

    @action.bound setInputPasswordReg(e : any) {
        this.inputPasswordReg = e.target.value;
    }


    @action.bound submitReg = () => {
    };
}
