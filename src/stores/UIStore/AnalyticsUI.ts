import {action, observable} from "mobx";
import UIStore from '../UIStore'

export default class AnalyticsUI {
    constructor(UIStore : UIStore) {
        this.UIStore = UIStore;
    }

    UIStore : UIStore;
}
