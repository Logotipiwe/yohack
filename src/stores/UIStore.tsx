import {action, observable} from "mobx";
import RootStore from './RootStore'
import HomeUI from "./UIStore/HomeUI";
import LoginUI from "./UIStore/LoginUI";
import AnalyticsUI from "./UIStore/AnalyticsUI";

class UIStore {
	constructor(RootStore : RootStore) {
		this.RootStore = RootStore;

		this.HomeUI = new HomeUI(this);
		this.AnalyticsUI = new AnalyticsUI(this);

		//после остальных UI
		this.LoginUI = new LoginUI(this);
	}

	RootStore : RootStore;
	HomeUI : HomeUI;
	AnalyticsUI: AnalyticsUI;
	LoginUI : LoginUI;

	@observable headerContextOpened : boolean = false;

	@observable modal = (process.env.NODE_ENV === 'development') ? null : null;

	@observable modalMeetingId : Nullable<number> = null;

	@action.bound setModal(val : any){
		this.modal = val;
	}

	@observable activeView : ActiveViews =
		(process.env.NODE_ENV === 'development') ? 'analytics' : 'home';

	@action.bound setActiveView = (view : ActiveViews) => {
		this.activeView = view;
		this.headerContextToggle();
	};

	@action.bound headerContextToggle(){
		this.headerContextOpened = !this.headerContextOpened;
	}
}

export default UIStore
