import * as React from 'react';
import '@vkontakte/vkui/dist/vkui.css';
import {inject, observer} from "mobx-react";
import RootStore from '../stores/RootStore'
import {
    Header,
    ModalPage, Div, Card, Textarea, Separator, Cell, List
} from '@vkontakte/vkui';
import dayjs from "dayjs";

@inject('RootStore')
@observer
class AnalyticsModalPage extends React.Component<{ RootStore?: RootStore, event : MeetingAnalytics, id : any}, {}> {
    render(): React.ReactElement {
        const RootStore = this.props.RootStore!;
        const {UIStore} = RootStore;
        const {EmulateStore} = RootStore;
        const event = this.props.event;

        const avgUseful = event.usefulGisto.reduce((sum,val,i)=>{
            return sum+(i+1)*val;
        },0)/event.usefulGisto.reduce((s,val)=>s+val,0);

        const avgTiming = event.timingGisto.reduce((sum,val,i)=>{
            return sum+(i+1)*val;
        },0)/event.timingGisto.reduce((s,val)=>s+val,0);

        return (
            <ModalPage
                id={'anal'+event.id}
                header={<Header aside={dayjs(event.time).format('D MMM hh:mm')}>{event.title}</Header>}
            >
                <Div>
                    <Header mode='secondary'>Описание</Header>
                    <Card size="l" mode='tint'>
                        <Div>
                            <p>Здесь описание встречи, ссылки, прикрепленные документы.</p>
                            <p>Ссылка на собрание:</p>
                            <p style={{color: 'var(--accent)'}}>https://skype.com/not_working_at_all</p>
                        </Div>
                    </Card>
                    <Header mode='secondary'>Цели меропритяия</Header>
                    <List>
                        <Cell selectable onChange={e=>{console.log(e.target.nodeValue);}}>Первая</Cell>
                        <Cell selectable>Вторая</Cell>
                        <Cell selectable>Третья</Cell>
                        <Cell selectable>и тд</Cell>
                    </List>
                    <Header mode='secondary'>Итоги встречи</Header>
                    <Card size="l" mode='tint'>
                        <Textarea placeholder='Опишите итоги встречи'/>
                    </Card>
                    <Header mode='secondary'>Аналитика</Header>
                    <Cell asideContent={event.wasConversion+'%'}>Сотрудников посетили</Cell>
                    <Cell asideContent={Math.round(avgUseful*10)/10}>Средняя оценка полезности</Cell>
                    <Cell asideContent={Math.round(avgTiming*10)/10}>Средняя оценка своевременности</Cell>
                </Div>
            </ModalPage>
        )
    }
}

export default AnalyticsModalPage;
