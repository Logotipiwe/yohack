import * as React from 'react';
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';
import '@vkontakte/vkui/dist/vkui.css';
import {inject, observer} from "mobx-react";
import RootStore from '../stores/RootStore'
import {

    Header,
    ModalPage,
    FormLayout,
    Radio, FormLayoutGroup, Div, Card
} from '@vkontakte/vkui';

@inject('RootStore')
@observer
class MeetingModalPage extends React.Component<{ RootStore?: RootStore, event : MeetingPast, id : any}, {}> {
    render(): React.ReactElement {
        const RootStore = this.props.RootStore!;
        const {UIStore} = RootStore;
        const {EmulateStore} = RootStore;
        const event = this.props.event;

        return (
            <ModalPage
                id={'eventMark'+event.id}
                key={'eventMark'+event.id}
                header={<Header>{event.title}</Header>}
            >
                <Div>
                    <Header mode='secondary'>Краткое описание</Header>
                    <Card size="l" mode='tint'>
                        <Div>Здесь описание встречи, ссылки, прикрепленные документы, цели и итоги встречи</Div>
                    </Card>
                </Div>
                <FormLayout>
                    <FormLayoutGroup top="Смогли ли вы участвовать?">
                        <Radio defaultChecked={(event.wasMark === 1)} onClick={EmulateStore.markMeeting.bind(null,event)} name="wasMark" value="1"><div style={{color: 'var(--button_commerce_background)'}}>Да</div></Radio>
                        <Radio defaultChecked={(event.wasMark === 0)} onClick={EmulateStore.markMeeting.bind(null,event)} name="wasMark" value="0"><div style={{color: 'red'}}>Нет</div></Radio>
                    </FormLayoutGroup>
                    <FormLayoutGroup top="Оцените полезность">
                        <Radio defaultChecked={(event.usefulMark === 5)} onClick={EmulateStore.markMeeting.bind(null,event)} name="usefulMark" value="5" description='Все цели были достигнуты'><div style={{color: 'var(--button_commerce_background)'}}>Отлично</div></Radio>
                        <Radio defaultChecked={(event.usefulMark === 4)} onClick={EmulateStore.markMeeting.bind(null,event)} name="usefulMark" value="4" description='Большинство целей достигнуты'>Полезно</Radio>
                        <Radio defaultChecked={(event.usefulMark === 3)} onClick={EmulateStore.markMeeting.bind(null,event)} name="usefulMark" value="3" description='Не все цели достигнуты или поставлены верно'>Удовлетворительно</Radio>
                        <Radio defaultChecked={(event.usefulMark === 2)} onClick={EmulateStore.markMeeting.bind(null,event)} name="usefulMark" value="2" description='Цели не были достигнуты'>Плохо</Radio>
                        <Radio defaultChecked={(event.usefulMark === 1)} onClick={EmulateStore.markMeeting.bind(null,event)} name="usefulMark" value="1"><div style={{color: 'red'}}>Бесполезно</div></Radio>
                    </FormLayoutGroup>
                    <FormLayoutGroup top="Оцените своевременность">
                        <Radio defaultChecked={(event.timingMark === 4)} onClick={EmulateStore.markMeeting.bind(null,event)} name="timingMark" value="4"><div style={{color: 'var(--button_commerce_background)'}}>Оптимально</div></Radio>
                        <Radio defaultChecked={(event.timingMark === 3)} onClick={EmulateStore.markMeeting.bind(null,event)} name="timingMark" value="3">Смогло вписаться в график</Radio>
                        <Radio defaultChecked={(event.timingMark === 2)} onClick={EmulateStore.markMeeting.bind(null,event)} name="timingMark" value="2">Пришлось подвинуть планы</Radio>
                        <Radio defaultChecked={(event.timingMark === 1)} onClick={EmulateStore.markMeeting.bind(null,event)} name="timingMark" value="1"><div style={{color: 'red'}}>Несвоевременно</div></Radio>
                    </FormLayoutGroup>
                </FormLayout>
            </ModalPage>
        )
    }
}

export default MeetingModalPage;
