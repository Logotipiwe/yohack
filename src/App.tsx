import * as React from 'react';
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';
import '@vkontakte/vkui/dist/vkui.css';
import Login from './Login'
import {inject, observer} from "mobx-react";
import RootStore from './stores/RootStore'
import {
    Root,
    View,
    ModalRoot,
} from '@vkontakte/vkui';
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "./PanelHeader";
import HomeManager from "./roots/HomeManager";
import AnalyticsManager from "./roots/AnalyticsManager";
import HomeStaff from './roots/HomeStaff';
import HistoryStaff from "./roots/HistoryStaff";
import MeetingModalPage from "./App/MeetingModalPage";
import AnalyticsModalPage from "./App/AnalyticsModalPage";

@inject('RootStore')
@observer
class App extends React.Component<{ RootStore?: RootStore }, {}> {
    componentDidMount(): void {
        // const schemeAttribute = document.createAttribute('scheme');
        // schemeAttribute.value = 'space_gray';
        // document.body.attributes.setNamedItem(schemeAttribute);
    }

    render(): React.ReactElement {
        const RootStore = this.props.RootStore!;
        const {UIStore} = RootStore;
        const {EmulateStore} = RootStore;

        if (!RootStore.auth) return <Login/>;
        if (!RootStore.dataLoaded) return <ScreenSpinner/>;

        const modal = <ModalRoot
            activeModal={UIStore.modal}
            onClose={UIStore.setModal.bind(null, null)}
        >
            {[
                ...EmulateStore.fakeStaffHistory.map((event) =>
                    <MeetingModalPage
                        id={'eventMark' + event.id}
                        key={'eventMark' + event.id}
                        event={event}
                    />
                ),
                ...EmulateStore.fakeManagerHistory.map((event) =>
                    <AnalyticsModalPage
                        id={'anal' + event.id}
                        key={'anal' + event.id}
                        event={event}
                    />
                ),
            ]}
        </ModalRoot>;

        return (
            <Root id='1' activeView='1'>
                <View
                    id='1'
                    activePanel={'1'}
                    header={false}
                    modal={modal}
                >
                    <Panel id='1' separator={false}>
                        <PanelHeader/>
                        {RootStore.isManager ?
                            UIStore.activeView === 'home' ? <HomeManager/> : <AnalyticsManager/>
                            :
                            UIStore.activeView === 'home' ? <HomeStaff/> : <HistoryStaff/>
                        }
                    </Panel>
                </View>
            </Root>
        )
    }
}

export default App;
