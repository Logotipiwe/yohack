import React from 'react';
import {Button, FormLayout, Header, Input, Link, Root, View} from "@vkontakte/vkui";
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import {inject, observer} from "mobx-react";
import RootStore from './stores/RootStore';

@inject("RootStore")
@observer
class Login extends React.Component<{RootStore?: RootStore},{}>{

	render() {
		const RootStore = this.props.RootStore!;
		const {LoginUI} = RootStore.UIStore;
		const EmulateStore = RootStore.EmulateStore;

		return (
			<Root activeView={LoginUI.activeView}>
				<View id='login' activePanel='1' header={false}>
					<Panel id='1'>
						<FormLayout>
							<Input top={'Логин'}
							       value={LoginUI.inputLogin}
							       onChange={LoginUI.setInputLogin}
							/>
							<Input top={'Пароль'}
							       value={LoginUI.inputPassword}
							       onChange={LoginUI.setInputPassword}
							/>
							<Button size="xl" mode="primary" onClick={EmulateStore.login}>Войти</Button>
							<Div style={{textAlign: 'center'}}>
								Ещё не зарегистрированы? <Link onClick={LoginUI.setActiveView.bind(null,'reg')}>Вам сюда</Link>
							</Div>
						</FormLayout>
					</Panel>
				</View>
				<View id='reg' activePanel='1' header={false}>
					<Panel id='1'>
						<FormLayout>
							<Header mode='primary'>Регистрация</Header>
							<Input top={'Логин'} value={LoginUI.inputLoginReg}
							       onChange={LoginUI.setInputLoginReg}/>
							<Input top={'Пароль'} value={LoginUI.inputPasswordReg}
							       onChange={LoginUI.setInputPasswordReg}
							/>
							<Input top={'Имя'} value={LoginUI.inputPasswordReg}
								   onChange={LoginUI.setInputPasswordReg}
							/>
							<Input top={'Фамилия'} value={LoginUI.inputPasswordReg}
								   onChange={LoginUI.setInputPasswordReg}
							/>
							<Input top={'Код организации'} value={LoginUI.inputPasswordReg}
								   onChange={LoginUI.setInputPasswordReg}
							/>
							<Button size="xl" mode="primary" onClick={LoginUI.submitReg}>Зарегистрироваться</Button>
							<Div style={{textAlign: 'center'}}>
								Уже зарегистрированы? <Link onClick={LoginUI.setActiveView.bind(null,'login')}>Вам сюда</Link>
							</Div>
						</FormLayout>
					</Panel>
				</View>
			</Root>
		);
	}
}
export default Login
