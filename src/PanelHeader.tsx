import PanelHeaderContent from "@vkontakte/vkui/dist/components/PanelHeaderContent/PanelHeaderContent";
import {PanelHeaderContext, PanelHeaderSimple, List, Cell, Counter} from "@vkontakte/vkui";
import Icon24Done from '@vkontakte/icons/dist/24/done';
import Icon16Dropdown from '@vkontakte/icons/dist/16/dropdown'
import Icon28UsersOutline from '@vkontakte/icons/dist/28/users_outline';
import Icon28SettingsOutline from '@vkontakte/icons/dist/28/list_outline';
import React from "react";
import {inject, observer} from "mobx-react";
import RootStore from "./stores/RootStore";

@inject("RootStore")
@observer
class PanelHeader extends React.Component<{ RootStore?: RootStore }, any> {
    render() {
        const RootStore = this.props.RootStore!;
        const {EmulateStore} = RootStore;
        const {HomeUI} = RootStore.UIStore;
        const {UIStore} = RootStore;

        return (
            <>
                <PanelHeaderSimple>
                    <PanelHeaderContent
                        status=''
                        aside={<Icon16Dropdown
                            style={{transform: `rotate(${UIStore.headerContextOpened ? '180deg' : '0'})`}}/>}
                        before=''
                        onClick={UIStore.headerContextToggle}>
                        {RootStore.isManager ? 'Организатор' : 'Сотрудник'}
                    </PanelHeaderContent>
                </PanelHeaderSimple>
                <PanelHeaderContext opened={UIStore.headerContextOpened} onClose={UIStore.headerContextToggle}>
                    <List>
                        {RootStore.isManager ? <>
                                <Cell
                                    before={<Icon28UsersOutline/>}
                                    asideContent={UIStore.activeView === 'home' ? <Icon24Done fill="var(--accent)"/> : null}
                                    onClick={UIStore.setActiveView.bind(null, 'home')}
                                >
                                    Мои встречи
                                </Cell>
                                <Cell
                                    before={<Icon28SettingsOutline/>}
                                    asideContent={UIStore.activeView === 'analytics' ?
                                        <Icon24Done fill="var(--accent)"/> : null}
                                    onClick={UIStore.setActiveView.bind(null, 'analytics')}
                                >
                                    Аналитика встреч
                                </Cell>
                                <Cell
                                    onClick={EmulateStore.logout}
                                ><div style={{color: 'var(--destructive)'}}>Выйти</div>
                                </Cell>
                            </> :
                            <>
                                <Cell
                                    before={<Icon28UsersOutline/>}
                                    indicator={
                                        EmulateStore.fakeStuffInvitesCount ?
                                        <Counter size="m" mode="prominent">{EmulateStore.fakeStuffInvitesCount}</Counter> : ''
                                    }
                                    asideContent={UIStore.activeView === 'home' ? <Icon24Done fill="var(--accent)"/> : null}
                                    onClick={UIStore.setActiveView.bind(null, 'home')}
                                >
                                    Мои приглашения
                                </Cell>
                                <Cell
                                    before={<Icon28SettingsOutline/>}
                                    asideContent={UIStore.activeView === 'analytics' ?
                                        <Icon24Done fill="var(--accent)"/> : null}
                                    onClick={UIStore.setActiveView.bind(null, 'analytics')}
                                >
                                    Прошедшие встречи
                                </Cell>
                                <Cell
                                    onClick={EmulateStore.logout}
                                ><div style={{color: 'var(--destructive)'}}>Выйти</div>
                                </Cell>
                            </>
                        }
                    </List>
                </PanelHeaderContext></>
        );
    }
}

export default PanelHeader
