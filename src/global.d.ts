type Nullable<T> = null | T;
type LoginView = "reg" | "login";
type ActiveViews = "home" | "analytics";
interface Meeting{
    id : number,
    title : string,
    time : Date
}
interface Invite extends Meeting{
    required: boolean
    accepted: Nullable<boolean>
}
interface MeetingPast extends Meeting{
    usefulMark? : number,
    timingMark? : number,
    wasMark? : number
}
interface MeetingAnalytics extends Meeting{
    wasConversion: number,
    usefulGisto: number[],
    timingGisto: number[]
}
